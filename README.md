**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

-----------------------------------------README-------------------------------------------------------------

# PLEASE WHEN YOU MAKE ANY CHANGE ON THIS PROJECT PLEASE DO NOT FORGET TO CREATE A BRANCH

1. To create a branch you will type on bach ou cmd ->  GIT CHECKOUT -B "__NOME DA BRANCH__"  EX: GIT CHECKOUT -B "ISSUE-CREATING-PROJECT", todo nome de brach começa com ISSUE-
2. Neste momento você cria uma branch e entra nela saindo automaticamente da master.
3. Apos todas as alterações necessarias siga esses passos para subir sua branch no git.
    3.1 GIT STATUS  -> Para visualizar as alteracoes
    3.2 GIT ADD .   -> Para adicionar na branch as alteracoes
    3.3 GIT COMMIT -M "E UM COMENTARIO ENTRE ASPAS PARA IDENTIFICAR AS MUDANCAS"
    3.4 GIT PUSH ORIGIN ISSUE-CREATING-PROJECT
4. Agora a branch estará disponivel para o merge no git
5. Entre no git confira as mudanças e aprove a branch, o ideal é esperar todos da equipe aprovarem a branch para entao ser alterada
6. Apos o merge todos precisam dar O comando GIT PULL MASTER para atualizar o projeto no computador com as alteraçoes

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

## Config da conta

Email: tccosbr@gmail.com
Senha: fatecipiranga

# Deploy in Firebase

Token: 1/d_F1vT4rqMjeUrP33-NOwAoxbnEZ1eSiJKM4sct8t0I

firebase deploy --token 1/d_F1vT4rqMjeUrP33-NOwAoxbnEZ1eSiJKM4sct8t0I