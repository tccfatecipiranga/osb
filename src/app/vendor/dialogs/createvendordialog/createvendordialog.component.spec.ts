import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatevendordialogComponent } from './createvendordialog.component';

describe('CreatevendordialogComponent', () => {
  let component: CreatevendordialogComponent;
  let fixture: ComponentFixture<CreatevendordialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatevendordialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatevendordialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
