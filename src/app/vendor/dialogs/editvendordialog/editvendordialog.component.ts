import { Component, OnInit, Inject, NgModule } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-seditdialog',
  templateUrl: './editvendordialog.component.html',
  styleUrls: ['./editvendordialog.component.css']
})
export class EditvendordialogComponent implements OnInit {
  form: FormGroup;
  nameOld: string;
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditvendordialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      id: this.data.id,
      name: this.data.name,
      nameOld: this.data.name,
      cnpj: this.data.cnpj,
      phone: this.data.phone,
      email: this.data.email,
      street: this.data.address.street,
      number: this.data.address.number,
      complement: this.data.address.complement,
      district: this.data.address.district,
      city: this.data.address.city,
      state: this.data.address.state
    });
  }
  
  submit(form) {
    this.dialogRef.close({
      id: this.data.id,
      name: form.value.name,
      cnpj: form.value.cnpj,
      phone: form.value.phone,
      email: form.value.email,
      address: {
        street: form.value.street,
        number: form.value.number,
        complement: form.value.complement,
        district: form.value.district,
        city: form.value.city,
        state: form.value.state
      }
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
