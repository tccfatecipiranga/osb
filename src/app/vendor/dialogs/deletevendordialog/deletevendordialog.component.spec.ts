import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletevendordialogComponent } from './deletevendordialog.component';

describe('DeletevendordialogComponent', () => {
  let component: DeletevendordialogComponent;
  let fixture: ComponentFixture<DeletevendordialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletevendordialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletevendordialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
