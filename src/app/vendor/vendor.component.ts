// Angular Components
  import { Component, OnInit, Inject, OnDestroy } from '@angular/core';

// Material Components
  import { MatTableDataSource } from '@angular/material';
  import { MatDialog } from '@angular/material';

// Firebase Components
  import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

// Custom Components
  import { EditvendordialogComponent } from './dialogs/editvendordialog/editvendordialog.component';
  import { CreatevendordialogComponent } from './dialogs/createvendordialog/createvendordialog.component';
  import { DeletevendordialogComponent } from './dialogs/deletevendordialog/deletevendordialog.component';

// etc
  import { VendorService } from './vendor.service';
  import { Vendor } from '../model/vendor.model';
  import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.css']
})

export class VendorComponent implements OnInit, OnDestroy {
  dataSource = new MatTableDataSource(); // variable for DataTable sa
  subs: Subscription; // variable for retrieval of data
  displayedColumns = ['name', 'cnpj', 'email', 'phone', 'created', 'actions']; // variable for layout arrangement

  constructor(
    public dialog: MatDialog, // variable used to instantiate dialog
    private ss: VendorService // variable for access to CRUD vendor of Vendors
  ) {}

  ngOnInit() {
    // Called once, on initialization of the instance
    this.subs = this.ss.fetchAvailableVendors(sessionStorage.getItem('userId')).subscribe(resp => {
      this.dataSource.data = resp; // Fill Data from subscription to data in dataSource
    });
    // console.log('Retrieval of data complete'); // Log Data Retrieval
  }
  ngOnDestroy(): void { // unsubscribe from vendorservice
    // Called once, before the instance is destroyed.
    this.subs.unsubscribe();
  }

  applyFilter(filterValue: string) { // Filter table
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  add() {
    const createDialog = this.dialog.open(CreatevendordialogComponent, { });
    createDialog.afterClosed().subscribe(vendors => {
      if (vendors) {
        // console.log(form.value.name); // Test return of Dialog
        this.ss.addVendor(vendors);
      }
    });
  }

  delete(id: string, name: string) {
    const deleteDialog = this.dialog.open(DeletevendordialogComponent, {
      data: {
        name: name
      }
    });
    deleteDialog.afterClosed().subscribe(resp => {
      if (resp) {
        // console.log(resp); // Test return of Dialog
        this.ss.deleteVendor(id);
      }
    });
  }

  update(i: number, vendor: Vendor) {
    const editDialog = this.dialog.open(EditvendordialogComponent, {
      data : vendor
    });
    editDialog.afterClosed().subscribe(vendors => {
      if (vendors) {
        // console.log(form); // Test return of Dialog
        this.ss.updateVendor(vendors);
      }
    });
  }

}
