import {Injectable} from '@angular/core';
import {Vendor} from '../model/vendor.model';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {MatTableDataSource} from '@angular/material';
import {Subject} from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from '@firebase/util';

@Injectable()
export class VendorService {
  vendorcollection: AngularFirestoreCollection<any> = this.afs.collection('users/' + sessionStorage.getItem('userId') + '/vendors/');

  constructor(
    private afs: AngularFirestore
  ) {}

  fetchAvailableVendors(userId: string): any {
    return this.afs
    .collection('users/' + userId + '/vendors/')
    .snapshotChanges()
    .map(items => {
      return items.map(doc => {
        return {
          id: doc.payload.doc.id,
          name: doc.payload.doc.data().name,
          cnpj: doc.payload.doc.data().cnpj,
          email: doc.payload.doc.data().email,
          phone: doc.payload.doc.data().phone,
          address: doc.payload.doc.data().address,
          createdDate: doc.payload.doc.data().createdDate
        };
      });
    });
  }

  addVendor(vendor: Vendor) {
    console.log(vendor); // Test JSON
    this.vendorcollection.add(vendor)
    .then(() => {
      console.log('Gravou Serviço'); // Test add vendor
    }).catch((err) => {
      console.log(err);
    });
  }

  updateVendor(vendor: Vendor) {
    // console.log(vendor); // Test JSON
    this.vendorcollection.doc(vendor.id).update(vendor).then(() => {
      console.log('Atualizou o Serviço'); // Test update vendor
    }).catch( (err) => {
      console.log(err); // Catch error update vendor
    });
  }

  deleteVendor(id: string) {
    // console.log(id); // Test JSON
    this.vendorcollection.doc(id).delete()
    .then(result => {
      // console.log(result); // Test delete vendor
    }).catch((err) => {
      // console.log(err); // Catch error delete vendor
    });
  }
}
