import {Injectable} from '@angular/core';
import {Product} from '../model/product.model';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';

@Injectable()
export class ProductService {
  productcollection: AngularFirestoreCollection<any> = this.afs.collection('users/' + sessionStorage.getItem('userId') + '/products/');

  constructor(
    private afs: AngularFirestore
  ) {
  }

  fetchAvailableProducts(userId: string): any {
    return this.afs
      .collection('users/' + userId + '/products/')
      .snapshotChanges()
      .map(items => {
        return items.map(doc => {
          return {
            id: doc.payload.doc.id,
            description: doc.payload.doc.data().description,
            model: doc.payload.doc.data().model,
            price: doc.payload.doc.data().price,
            priceCost: doc.payload.doc.data().priceCost,
            brand: doc.payload.doc.data().brand,
            type: doc.payload.doc.data().type,
            createdDate: doc.payload.doc.data().createdDate
          };
        });
      });
  }

  addProduct(product: Product) {
     console.log(product);
    this.productcollection.add(product)
      .then(() => {
        console.log('Gravou Produto'); // Test add product
      }).catch((err) => {
      console.log(err);
    });
  }

  updateProduct(product: Product) {
    console.log(product); // Test JSON
    this.productcollection.doc(product.id).update(product)
    .then(() => {
      console.log('Atualizou o Produto'); // Test update product
    }).catch((err) => {
      console.log(err); // Catch error update product
    });
  }

  deleteProduct(id: string) {
    // console.log(id); // Test JSON
    this.productcollection.doc(id).delete()
      .then(result => {
        // console.log(result); // Test delete product
      }).catch((err) => {
      console.log(err); // Catch error delete product
    });
  }
}
