import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {EditProductDialogComponent} from './dialogs/editProductDialog/editProductDialog.component';
import {CreateProductDialogComponent} from './dialogs/createProductDialog/createProductDialog.component';
import {DeleteProductDialogComponent} from './dialogs/deleteProductDialog/deleteProductDialog.component';
import {ProductService} from './product.service';
import {Subscription} from 'rxjs/Subscription';
import {AuthService} from '../auth/auth.service';
import {Product} from '../model/product.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit, OnDestroy {
  dataSource = new MatTableDataSource(); // variable for DataTable sa
  subs: Subscription; // variable for retrieval of data
  displayedColumns = ['model', 'brand', 'price', 'created', 'actions']; // variable for layout arrangement

  constructor(
    private dialog: MatDialog, // variable used to instantiate dialog
    private productService: ProductService, // variable for access to CRUD product of Products
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    this.subs = this.productService.fetchAvailableProducts(sessionStorage.getItem('userId')).subscribe(resp => {
      this.dataSource.data = resp; // Fill Data from subscription to data in dataSource
    });
  }

  ngOnDestroy(): void { // unsubscribe from productproduct
    this.subs.unsubscribe();
  }

  applyFilter(filterValue: string) { // Filter table
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  add() {
    const createDialog = this.dialog.open(CreateProductDialogComponent, {});
    createDialog.afterClosed().subscribe(product => {
      if (product) {
        this.productService.addProduct(product);
      }
    });
  }

  delete(id: string, model: string) {
    const deleteDialog = this.dialog.open(DeleteProductDialogComponent, {
      data: {
        model: model
      }
    });
    deleteDialog.afterClosed().subscribe(resp => {
      if (resp) {
        this.productService.deleteProduct(id);
      }
    });
  }

  update(i: number, product: Product) {
    this.authService.getId();
    console.log(product);

    const editDialog = this.dialog.open(EditProductDialogComponent, {
      data: product
    });
    // tslint:disable-next-line:no-shadowed-variable
    editDialog.afterClosed().subscribe(product => {
      if (product) {
        this.productService.updateProduct(product);
      }
    });
  }

}
