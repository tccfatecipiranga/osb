import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialogRef} from '@angular/material';
import {BrandService} from '../../../brand/brand.service';
import {FirebaseListObservable} from 'angularfire2/database-deprecated';
import {Brand} from '../../../model/brand.model';
import {ProductType} from '../../../model/productType.model';
import {ProductTypeService} from '../../../productType/productType.service';

@Component({
  selector: 'app-createproductdialog',
  templateUrl: './createProductDialog.component.html',
  styleUrls: ['./createProductDialog.component.css']
})
export class CreateProductDialogComponent implements OnInit {
  form: FormGroup;
  brands: FirebaseListObservable<Brand[]>;
  types: FirebaseListObservable<ProductType[]>;
  private selectedBrand: Brand;
  private selectedType: ProductType;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<CreateProductDialogComponent>,
    private brandService: BrandService,
    private productTypeService: ProductTypeService
  ) {
    this.getBrands();
    this.getProductTypes();
  }

  getBrands() {
      this.brands = this.brandService.fetchAvailableBrands(sessionStorage.getItem('userId'));
  }

  private getProductTypes() {
    this.types = this.productTypeService.fetchAvailableProductTypes(sessionStorage.getItem('userId'));
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ''
    });
  }

  submit(form) {
    this.dialogRef.close({
      model: form.value.model,
      description: form.value.description,
      price: form.value.price,
      priceCost: form.value.priceCost,
      brand: this.selectedBrand,
      type: this.selectedType,
      createdDate: Date.now()
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  checkBrand(brand: Brand) {
    this.selectedBrand = brand;
  }

  checkType(type: ProductType) {
    this.selectedType = type;
  }
}
