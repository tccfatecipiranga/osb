import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProductType } from '../../../model/productType.model';
import { FirebaseListObservable } from 'angularfire2/database-deprecated';
import { Brand } from '../../../model/brand.model';
import { BrandService } from '../../../brand/brand.service';
import { ProductTypeService } from '../../../productType/productType.service';
import { Product } from '../../../model/product.model';

@Component({
  selector: 'app-seditdialog',
  templateUrl: './EditProductDialog.component.html',
  styleUrls: ['./EditProductDialog.component.css']
})
export class EditProductDialogComponent implements OnInit {
  form: FormGroup;
  descriptionOld: string;
  brands: FirebaseListObservable<Brand[]>;
  types: FirebaseListObservable<ProductType[]>;
  selectedBrand: Brand;
  selectedType: ProductType;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditProductDialogComponent>,
    private brandService: BrandService,
    private productTypeService: ProductTypeService,
    @Inject(MAT_DIALOG_DATA) private data
  ) {
    this.getBrands();
    this.getProductTypes();
  }

  getBrands() {
    this.brands = this.brandService.fetchAvailableBrands(sessionStorage.getItem('userId'));
  }

  getProductTypes() {
    this.types = this.productTypeService.fetchAvailableProductTypes(sessionStorage.getItem('userId'));
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      id: this.data.id,
      description: this.data.description,
      descriptionOld: this.data.name,
      model: this.data.model,
      price: this.data.price,
      priceCost: this.data.priceCost,
      brand: this.data.brand,
      type: this.data.type
    });
    this.selectedBrand = this.data.brand;
    this.selectedType = this.data.type;
  }

  submit(form) {
    this.dialogRef.close({
      id: this.data.id,
      model: form.value.model,
      description: form.value.description,
      price: form.value.price,
      priceCost: form.value.priceCost,
      brand: this.selectedBrand,
      type: this.selectedType
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  checkBrand(brand: Brand) {
    this.selectedBrand = brand;

    // this.selectedBrand = this.brandService.findById(sessionStorage.getItem('userId'), brandId);
  }

  checkType(type: ProductType) {
    this.selectedType = type;
  }

  compareBrandFn(b1: Brand, b2: Brand): boolean { return b1 && b2 ? b1.id === b2.id : b1 === b2; }
  compareProductTypeFn(t1: ProductType, t2: ProductType): boolean { return t1 && t2 ? t1.id === t2.id : t1 === t2; }
}
