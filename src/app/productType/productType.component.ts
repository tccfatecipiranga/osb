import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {EditProductTypeDialogComponent} from './dialogs/editProductTypeDialog/editProductTypeDialog.component';
import {CreateProductTypeDialogComponent} from './dialogs/createProductTypeDialog/createProductTypeDialog.component';
import {DeleteProductTypeDialogComponent} from './dialogs/deleteProductTypeDialog/deleteProductTypeDialog.component';
import {ProductTypeService} from './productType.service';
import {Subscription} from 'rxjs/Subscription';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-producttype',
  templateUrl: './productType.component.html',
  styleUrls: ['./productType.component.css']
})

export class ProductTypeComponent implements OnInit, OnDestroy {
  dataSource = new MatTableDataSource(); // variable for DataTable sa
  subs: Subscription; // variable for retrieval of data
  displayedColumns = ['name', 'created', 'actions']; // variable for layout arrangement

  constructor(
    private dialog: MatDialog, // variable used to instantiate dialog
    private productTypeService: ProductTypeService, // variable for access to CRUD productType of ProductTypes
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    this.subs = this.productTypeService.fetchAvailableProductTypes(sessionStorage.getItem('userId')).subscribe(resp => {
      this.dataSource.data = resp; // Fill Data from subscription to data in dataSource
    });
  }

  ngOnDestroy(): void { // unsubscribe from productTypeproductType
    this.subs.unsubscribe();
  }

  applyFilter(filterValue: string) { // Filter table
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  add() {
    const createDialog = this.dialog.open(CreateProductTypeDialogComponent, {});
    createDialog.afterClosed().subscribe(productType => {
      if (productType) {
        this.productTypeService.addProductType(productType);
      }
    });
  }

  delete(id: string, name: string) {
    const deleteDialog = this.dialog.open(DeleteProductTypeDialogComponent, {
      data: {
        name: name
      }
    });
    deleteDialog.afterClosed().subscribe(resp => {
      if (resp) {
        this.productTypeService.deleteProductType(id);
      }
    });
  }

  update(i: number, id: string, name: string, value: number) {
    this.authService.getId();
    const editDialog = this.dialog.open(EditProductTypeDialogComponent, {
      data: {
        id: id,
        name: name,
        value: value
      }
    });
    editDialog.afterClosed().subscribe(productType => {
      if (productType) {
        this.productTypeService.updateProductType(productType);
      }
    });
  }


}
