import {Injectable} from '@angular/core';
import {ProductType} from '../model/productType.model';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';

@Injectable()
export class ProductTypeService {
  productTypecollection: AngularFirestoreCollection<any> = this.afs.collection('users/' + sessionStorage.getItem('userId') + '/productTypes/');

  constructor(
    private afs: AngularFirestore
  ) {
  }

  fetchAvailableProductTypes(userId: string): any {
    return this.afs
      .collection('users/' + userId + '/productTypes/')
      .snapshotChanges()
      .map(items => {
        return items.map(doc => {
          return {
            id: doc.payload.doc.id,
            name: doc.payload.doc.data().name,
            createdDate: doc.payload.doc.data().createdDate
          };
        });
      });
  }

  addProductType(productType: ProductType) {
    this.productTypecollection.add(productType)
      .then(() => {
        console.log('Gravou Tipo de Produto'); // Test add productType
      }).catch((err) => {
      console.log(err);
    });
  }

  updateProductType(productType: ProductType) {
    // console.log(productType); // Test JSON
    this.productTypecollection.doc(productType.id).update(productType).then(() => {
      console.log('Atualizou o Tipo de Produto'); // Test update productType
    }).catch((err) => {
      console.log(err); // Catch error update productType
    });
  }

  deleteProductType(id: string) {
    // console.log(id); // Test JSON
    this.productTypecollection.doc(id).delete()
      .then(result => {
        // console.log(result); // Test delete productType
      }).catch((err) => {
      console.log(err); // Catch error delete productType
    });
  }
}
