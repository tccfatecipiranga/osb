import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteProductTypeDialogComponent } from './deleteProductTypeDialog.component';

describe('DeleteProductTypeDialogComponent', () => {
  let component: DeleteProductTypeDialogComponent;
  let fixture: ComponentFixture<DeleteProductTypeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteProductTypeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteProductTypeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
