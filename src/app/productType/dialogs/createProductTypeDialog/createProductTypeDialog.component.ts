import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-createproducttypedialog',
  templateUrl: './createProductTypeDialog.component.html',
  styleUrls: ['./createProductTypeDialog.component.css']
})
export class CreateProductTypeDialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<CreateProductTypeDialogComponent>
  ) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ''
    });
  }

  submit(form) {
    this.dialogRef.close({
      name: form.value.name,
      createdDate: Date.now()
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
