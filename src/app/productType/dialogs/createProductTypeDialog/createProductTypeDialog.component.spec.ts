import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateProductTypeDialogComponent} from './createProductTypeDialog.component';

describe('CreateProductTypeDialogComponent', () => {
  let component: CreateProductTypeDialogComponent;
  let fixture: ComponentFixture<CreateProductTypeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateProductTypeDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProductTypeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
