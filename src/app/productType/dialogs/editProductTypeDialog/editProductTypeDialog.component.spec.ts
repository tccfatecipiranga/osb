import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProductTypeDialogComponent } from './editProductTypeDialog.component';

describe('EditProductTypeDialogComponent', () => {
  let component: EditProductTypeDialogComponent;
  let fixture: ComponentFixture<EditProductTypeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProductTypeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProductTypeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
