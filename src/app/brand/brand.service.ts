import {Injectable} from '@angular/core';
import {Brand} from '../model/brand.model';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';

@Injectable()
export class BrandService {
  brandcollection: AngularFirestoreCollection<any> = this.afs.collection('users/' + sessionStorage.getItem('userId') + '/brands/');

  constructor(
    private afs: AngularFirestore
  ) {
  }

  fetchAvailableBrands(userId: string): any {
    return this.afs
      .collection('users/' + userId + '/brands/')
      .snapshotChanges()
      .map(items => {
        return items.map(doc => {
          return {
            id: doc.payload.doc.id,
            name: doc.payload.doc.data().name,
            createdDate: doc.payload.doc.data().createdDate
          };
        });
      });
  }

  /* findById(userId: string, brandId: string): any {
    return this.afs.doc('users/' + userId + '/brands/' + brandId)
      .snapshotChanges()
      .map(items => {
        return items.map(doc => {
          return {
            id: doc.payload.doc.id,
            name: doc.payload.doc.data().name,
            createdDate: doc.payload.doc.data().createdDate
          };
        });
      });
  } */

  addBrand(brand: Brand) {
    this.brandcollection.add(brand)
      .then(() => {
        console.log('Gravou Marca'); // Test add brand
      }).catch((err) => {
      console.log(err);
    });
  }

  updateBrand(brand: Brand) {
    // console.log(brand); // Test JSON
    this.brandcollection.doc(brand.id).update(brand).then(() => {
      console.log('Atualizou o Marca'); // Test update brand
    }).catch((err) => {
      console.log(err); // Catch error update brand
    });
  }

  deleteBrand(id: string) {
    // console.log(id); // Test JSON
    this.brandcollection.doc(id).delete()
      .then(result => {
        // console.log(result); // Test delete brand
      }).catch((err) => {
      console.log(err); // Catch error delete brand
    });
  }
}
