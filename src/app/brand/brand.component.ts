import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {EditbranddialogComponent} from './dialogs/editbranddialog/editbranddialog.component';
import {CreatebranddialogComponent} from './dialogs/createbranddialog/createbranddialog.component';
import {DeletebranddialogComponent} from './dialogs/deletebranddialog/deletebranddialog.component';
import {BrandService} from './brand.service';
import {Subscription} from 'rxjs/Subscription';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})

export class BrandComponent implements OnInit, OnDestroy {
  dataSource = new MatTableDataSource(); // variable for DataTable sa
  subs: Subscription; // variable for retrieval of data
  displayedColumns = ['name', 'created', 'actions']; // variable for layout arrangement
  // uid = this.authService.getId;

  constructor(
    private dialog: MatDialog, // variable used to instantiate dialog
    private brandService: BrandService, // variable for access to CRUD brand of Brands
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    // Called once, on initialization of the instance
    this.subs = this.brandService.fetchAvailableBrands(sessionStorage.getItem('userId')).subscribe(resp => {
      this.dataSource.data = resp; // Fill Data from subscription to data in dataSource
    });
    // console.log('Retrieval of data complete'); // Log Data Retrieval
  }

  ngOnDestroy(): void { // unsubscribe from brandbrand
    this.subs.unsubscribe();
  }

  applyFilter(filterValue: string) { // Filter table
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  add() {
    const createDialog = this.dialog.open(CreatebranddialogComponent, {});
    createDialog.afterClosed().subscribe(brand => {
      if (brand) {
        this.brandService.addBrand(brand);
      }
    });
  }

  delete(id: string, name: string) {
    const deleteDialog = this.dialog.open(DeletebranddialogComponent, {
      data: {
        name: name
      }
    });
    deleteDialog.afterClosed().subscribe(resp => {
      if (resp) {
        this.brandService.deleteBrand(id);
      }
    });
  }

  update(i: number, id: string, name: string, value: number) {
    this.authService.getId();
    const editDialog = this.dialog.open(EditbranddialogComponent, {
      data: {
        id: id,
        name: name,
        value: value
      }
    });
    editDialog.afterClosed().subscribe(brand => {
      if (brand) {
        this.brandService.updateBrand(brand);
      }
    });
  }


}
