import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditbranddialogComponent } from './editbranddialog.component';

describe('EditbranddialogComponent', () => {
  let component: EditbranddialogComponent;
  let fixture: ComponentFixture<EditbranddialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditbranddialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditbranddialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
