import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-seditdialog',
  templateUrl: './editbranddialog.component.html',
  styleUrls: ['./editbranddialog.component.css']
})
export class EditbranddialogComponent implements OnInit {
  form: FormGroup;
  nameOld: string;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditbranddialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      id: this.data.id,
      name: this.data.name,
      nameOld: this.data.name,
    });
  }

  submit(form) {
    this.dialogRef.close({
      id: this.data.id,
      name: form.value.name,
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
