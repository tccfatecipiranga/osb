import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletebranddialogComponent } from './deletebranddialog.component';

describe('DeletebranddialogComponent', () => {
  let component: DeletebranddialogComponent;
  let fixture: ComponentFixture<DeletebranddialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletebranddialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletebranddialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
