import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-createbranddialog',
  templateUrl: './createbranddialog.component.html',
  styleUrls: ['./createbranddialog.component.css']
})
export class CreatebranddialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<CreatebranddialogComponent>
  ) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ''
    });
  }

  submit(form) {
    this.dialogRef.close({
      name: form.value.name,
      createdDate: Date.now()
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
