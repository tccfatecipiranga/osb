import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatebranddialogComponent } from './createbranddialog.component';

describe('CreatebranddialogComponent', () => {
  let component: CreatebranddialogComponent;
  let fixture: ComponentFixture<CreatebranddialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatebranddialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatebranddialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
