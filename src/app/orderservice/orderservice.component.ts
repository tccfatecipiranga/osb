// Angular Components
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';

// Material Components
  import { MatTableDataSource } from '@angular/material';
  import { MatDialog } from '@angular/material';

// Firebase Components
  import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

// Custom Components
  import { CreateorderserviceDialogComponent } from './dialogs/createorderservicedialog/createorderservicedialog.component';
  import { EditorderserviceDialogComponent} from './dialogs/editorderservicedialog/editorderservicedialog.component';
  import { DeleteorderserviceDialogComponent } from './dialogs/deleteorderservicedialog/deleteorderservicedialog.component';

// etc
  import { Subscription } from 'rxjs/Subscription';
  import { AuthService } from '../auth/auth.service';
import {OrderserviceService} from './orderservice.service';

@Component({
  selector: 'app-orderservice',
  templateUrl: './orderservice.component.html',
  styleUrls: ['./orderservice.component.css']
})
export class OrderserviceComponent implements OnInit, OnDestroy {
  dataSource = new MatTableDataSource(); // variable for DataTable sa
  subs: Subscription; // variable for retrieval of data
  displayedColumns = ['nos', 'clientName', 'productModel', 'created', 'totalValue', 'actions']; // variable for layout arrangement

  constructor(
    private dialog: MatDialog, // variable used to instantiate dialog
    private authService: AuthService,
    private orderServiceService: OrderserviceService
  ) {}

  ngOnInit() {
    // Called once, on initialization of the instance
    this.subs = this.orderServiceService.fetchAvailableServiceOrders(sessionStorage.getItem('userId')).subscribe(resp => {
     this.dataSource.data = resp; // Fill Data from subscription to data in dataSource
    });
    // console.log('Retrieval of data complete'); // Log Data Retrieval
  }
  ngOnDestroy(): void { // unsubscribe from serviceservice
    // Called once, before the instance is destroyed.
    this.subs.unsubscribe();
  }

  applyFilter(filterValue: string) { // Filter table
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  add() {
    console.log(this.dataSource);
    const createDialog = this.dialog.open(CreateorderserviceDialogComponent, {});
    createDialog.afterClosed().subscribe(orderService => {
      console.log(orderService);
      this.orderServiceService.addOrderService(orderService);
    });
  }

  update(element) {
    console.log(element);
    const editDialog = this.dialog.open(EditorderserviceDialogComponent, {
      data: {
        id: element.id,
        client: element.client,
        type: element.typeProduct,
        brand: element.brand,
        model: element.model,
        serial: element.serial,
        problem: element.problem,
        descProblem: element.descProblem,
        totalValue: element.totalValue,
        solution: element.solution
      }
    });
    editDialog.afterClosed().subscribe( orderService => {
      // console.log(orderService);
      this.orderServiceService.updateOrderService(orderService);
    });

  }

}
