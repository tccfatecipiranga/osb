import {Injectable} from '@angular/core';
import {ServiceOrder} from '../model/serviceorder.model';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from 'angularfire2/firestore';
import {MatTableDataSource} from '@angular/material';
import {Subject} from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from '@firebase/util';

@Injectable()
export class OrderserviceService {
  servicecollection: AngularFirestoreCollection<any> = this.afs.collection('users/' + sessionStorage.getItem('userId') + '/serviceorders/');
  // serviceChanged = new Subject<Service>();
  // servicesChanged = new Subject<Service[]>();
  // private services: Service[] = [];
  nos: AngularFirestoreDocument<number>;
  constructor(
    private afs: AngularFirestore
  ) {}

  fetchAvailableServiceOrders(userId: string): any {
    return this.afs
    .collection('users/' + userId + '/serviceorders/')
    .snapshotChanges()
    .map(items => {
      return items.map(doc => {
        console.log(doc.payload.doc.data()); // test retrive of items
        return { // Colocar dados da OS
          id: doc.payload.doc.id,
          client: doc.payload.doc.data().client,
          typeProduct: doc.payload.doc.data().typeProduct,
          brand: doc.payload.doc.data().brand,
          model: doc.payload.doc.data().model,
          serial: doc.payload.doc.data().serial,
          problem: doc.payload.doc.data().problem,
          approved: doc.payload.doc.data().approved,
          descProblem: doc.payload.doc.data().descProblem,
          solution: doc.payload.doc.data().solution,
          products: doc.payload.doc.data().products,
          services: doc.payload.doc.data().services,
          taxExtra: doc.payload.doc.data().taxExtra,
          totalValue: doc.payload.doc.data().totalValue,
          createdDate: doc.payload.doc.data().createdDate
        };
      });
    });
    // .subscribe((resp: Service[]) => {
    //   this.services = resp;
    //   this.servicesChanged.next([...this.services]);
    // });
  }

  addOrderService(orderService: ServiceOrder) {
    // console.log(service); // Test JSON
    console.log(orderService);

    // this.nos = this.afs.collection('users/' + sessionStorage.getItem('userId')).doc('nos');
    // console.log(this.nos);
    // const os: ServiceOrder = {
    //   client: orderService.client,
    //   typeProduct: orderService.typeProduct,
    //   brand: orderService.brand,
    //   serial: orderService.serial,
    //   problem: orderService.problem
    // };
    // console.log(os);
    this.servicecollection.add(orderService)
    .then(() => {
      console.log('Gravou Ordem de Serviço'); // Test add service
    }).catch((err) => {
      console.log('Deu ruim');
      console.log(err);
    });
  }

  updateOrderService(orderservice: ServiceOrder) {
    // console.log(service); // Test JSON
    this.servicecollection.doc(orderservice.id).update(orderservice)
    .then(() => {
      console.log('Atualizou o Serviço'); // Test update service
    }).catch( (err) => {
      console.log(err); // Catch error update service
    });
  }

  deleteOrderService(id: string) {
    // console.log(id); // Test JSON
    this.servicecollection.doc(id).delete()
    .then(result => {
      // console.log(result); // Test delete service
    }).catch((err) => {
      // console.log(err); // Catch error delete service
    });
  }
}
