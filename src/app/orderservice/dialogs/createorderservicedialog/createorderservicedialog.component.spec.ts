import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateorderserviceComponent } from './createorderservicedialog.component';

describe('CreateorderserviceComponent', () => {
  let component: CreateorderserviceComponent;
  let fixture: ComponentFixture<CreateorderserviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateorderserviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateorderserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
