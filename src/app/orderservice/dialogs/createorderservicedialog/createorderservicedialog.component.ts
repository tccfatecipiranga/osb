import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialog, MatDialogRef} from '@angular/material';
import {FirebaseListObservable} from 'angularfire2/database-deprecated';
import {Brand} from '../../../model/brand.model';
import {ProductType} from '../../../model/productType.model';
import {ProductTypeService} from '../../../productType/productType.service';
import {BrandService} from '../../../brand/brand.service';
import {SearchclientdialogComponent} from '../../../client/dialogs/searchclientdialog/searchclientdialog.component';
import {Client} from '../../../model/client.model';

@Component({
  selector: 'app-createorderservicedialog',
  templateUrl: './createorderservicedialog.component.html',
  styleUrls: ['./createorderservicedialog.component.css']
})
export class CreateorderserviceDialogComponent implements OnInit {
  typePerson = '';
  form: FormGroup;
  brands: FirebaseListObservable<Brand[]>;
  types: FirebaseListObservable<ProductType[]>;
  selectedBrand: Brand;
  selectedType: ProductType;
  selectedClient: Client;
  clientName = '';

  constructor(
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<CreateorderserviceDialogComponent>,
    private brandService: BrandService,
    private productTypeService: ProductTypeService
  ) {
    this.getBrands();
    this.getProductTypes();
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: '',
      value: ''
    });
  }

  getBrands() {
    this.brands = this.brandService.fetchAvailableBrands(sessionStorage.getItem('userId'));
  }

  getProductTypes() {
  this.types = this.productTypeService.fetchAvailableProductTypes(sessionStorage.getItem('userId'));
  }

  submit(form) {
    this.dialogRef.close({
      client: this.selectedClient,
      typeProduct: this.selectedType,
      brand: this.selectedBrand,
      model: form.value.model,
      serial: form.value.serial,
      problem: form.value.problem,
      createdDate: Date.now()
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  selectClient() {
    const selClientDialog = this.dialog.open(SearchclientdialogComponent, {});
    selClientDialog.afterClosed().subscribe(resp => {
      this.selectedClient = resp.selectedClient;
      this.clientName = this.selectedClient.name;
    });
  }

  checkBrand(brand: Brand) {
    this.selectedBrand = brand;
  }

  checkType(type: ProductType) {
    this.selectedType = type;
  }
}
