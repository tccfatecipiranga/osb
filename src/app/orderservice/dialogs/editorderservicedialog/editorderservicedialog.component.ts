import { Component, OnInit, Inject, NgModule } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import {Brand} from '../../../model/brand.model';
import {SearchclientdialogComponent} from '../../../client/dialogs/searchclientdialog/searchclientdialog.component';
import {ProductType} from '../../../model/productType.model';
import {Client} from '../../../model/client.model';
import {FirebaseListObservable} from 'angularfire2/database-deprecated';
import {ProductTypeService} from '../../../productType/productType.service';
import {CreateorderserviceDialogComponent} from '../createorderservicedialog/createorderservicedialog.component';
import {BrandService} from '../../../brand/brand.service';

@Component({
  selector: 'app-seditorderdialog',
  templateUrl: './editorderservicedialog.component.html',
  styleUrls: ['./editorderservicedialog.component.css']
})
export class EditorderserviceDialogComponent implements OnInit {
  typePerson = '';
  form: FormGroup;
  brands: FirebaseListObservable<Brand[]>;
  types: FirebaseListObservable<ProductType[]>;
  selectedBrand: Brand;
  selectedType: ProductType;
  selectedClient: Client;
  clientName = '';
  constructor(
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<CreateorderserviceDialogComponent>,
    private brandService: BrandService,
    private productTypeService: ProductTypeService,
    @Inject(MAT_DIALOG_DATA) private data
  ) {
    this.getBrands();
    this.getProductTypes();
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      model: this.data.model,
      serial: this.data.serial,
      problem: this.data.problem,
      type: this.data.type,
      brand: this.data.brand,
      descProblem: this.data.descProblem,
      solution: this.data.solution,
      totalValue: this.data.totalValue
    });
    this.clientName = this.data.client.name;
    this.selectedClient = this.data.client;
    this.selectedBrand = this.data.brand;
    this.selectedType = this.data.type;
  }
  getBrands() {
    this.brands = this.brandService.fetchAvailableBrands(sessionStorage.getItem('userId'));
  }

  getProductTypes() {
    this.types = this.productTypeService.fetchAvailableProductTypes(sessionStorage.getItem('userId'));
  }
  submit(form) {
    this.dialogRef.close({
      id: this.data.id,
      client: this.selectedClient,
      model: form.value.model,
      serial: form.value.serial,
      problem: form.value.problem,
      type: this.selectedType,
      brand: this.selectedBrand,
      descProblem: form.value.descProblem,
      solution: form.value.solution,
      totalValue: form.value.totalValue
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  checkBrand(brand: Brand) {
    this.selectedBrand = brand;
  }

  checkType(type: ProductType) {
    this.selectedType = type;
  }

  compareBrandFn(b1: Brand, b2: Brand): boolean { return b1 && b2 ? b1.id === b2.id : b1 === b2; }
  compareProductTypeFn(t1: ProductType, t2: ProductType): boolean { return t1 && t2 ? t1.id === t2.id : t1 === t2; }
}
