import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorderservicedialogComponent } from './editorderservicedialog.component';

describe('EditorderservicedialogComponent', () => {
  let component: EditorderservicedialogComponent;
  let fixture: ComponentFixture<EditorderservicedialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorderservicedialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorderservicedialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
