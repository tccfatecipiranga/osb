import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteorderserviceComponent } from './deleteorderservicedialog.component';

describe('DeleteorderserviceComponent', () => {
  let component: DeleteorderserviceComponent;
  let fixture: ComponentFixture<DeleteorderserviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteorderserviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteorderserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
