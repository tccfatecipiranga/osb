// Angular Components
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';

// Material Components
import { MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';

// Firebase Components
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

// Custom Components
import { EditclientdialogComponent } from './dialogs/editclientdialog/editclientdialog.component';
import { CreateclientdialogComponent } from './dialogs/createclientdialog/createclientdialog.component';
import { DeleteclientdialogComponent } from './dialogs/deleteclientdialog/deleteclientdialog.component';

// etc
import { ClientService } from './client.service';
import { Client } from '../model/client.model';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})

export class ClientComponent implements OnInit, OnDestroy {
  dataSource = new MatTableDataSource(); // variable for DataTable sa
  subs: Subscription; // variable for retrieval of data
  displayedColumns = ['name', 'document', 'email', 'phone', 'created', 'actions']; // variable for layout arrangement

  constructor(
    public dialog: MatDialog, // variable used to instantiate dialog
    private ss: ClientService // variable for access to CRUD client of clients
  ) { }

  ngOnInit() {
    // Called once, on initialization of the instance
    this.subs = this.ss.fetchAvailableClients(sessionStorage.getItem('userId')).subscribe(resp => {
      this.dataSource.data = resp; // Fill Data from subscription to data in dataSource
    });
    // console.log('Retrieval of data complete'); // Log Data Retrieval
  }
  ngOnDestroy(): void { // unsubscribe from vendorservice
    // Called once, before the instance is destroyed.
    this.subs.unsubscribe();
  }

  applyFilter(filterValue: string) { // Filter table
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  add() {
    const createDialog = this.dialog.open(CreateclientdialogComponent, {});
    createDialog.afterClosed().subscribe(clients => {
      if (clients) {
        // console.log(form.value.name); // Test return of Dialog
        this.ss.addClient(clients);
      }
    });
  }

  delete(id: string, name: string) {
    const deleteDialog = this.dialog.open(DeleteclientdialogComponent, {
      data: {
        name: name
      }
    });
    deleteDialog.afterClosed().subscribe(resp => {
      if (resp) {
        // console.log(resp); // Test return of Dialog
        this.ss.deleteClient(id);
      }
    });
  }

  update(i: number, client: Client) {
    const editDialog = this.dialog.open(EditclientdialogComponent, {
      data : client
    });
    editDialog.afterClosed().subscribe(clients => {
      if (clients) {
        // console.log(form); // Test return of Dialog
        this.ss.updateClient(clients);
      }
    });
  }

}


