import { Injectable } from '@angular/core';
import { Client } from '../model/client.model';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { MatTableDataSource } from '@angular/material';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from '@firebase/util';

@Injectable()
export class ClientService {
  clientcollection: AngularFirestoreCollection<any> = this.afs.collection('users/' + sessionStorage.getItem('userId') + '/clients/');

  constructor(
    private afs: AngularFirestore
  ) { }

  fetchAvailableClients(userId: string): any {
    return this.afs
      .collection('users/' + userId + '/clients/')
      .snapshotChanges()
      .map(items => {
        return items.map(doc => {
          return {
            id: doc.payload.doc.id,
            name: doc.payload.doc.data().name,
            document: doc.payload.doc.data().cnpj,
            email: doc.payload.doc.data().email,
            phone: doc.payload.doc.data().phone,
            address: doc.payload.doc.data().address,
            createdDate: doc.payload.doc.data().createdDate
          };
        });
      });
  }

  addClient(client: Client) {
    // console.log(client); // Test JSON
    this.clientcollection.add(client)
      .then(() => {
        console.log('Gravou Serviço'); // Test add client
      }).catch((err) => {
        console.log(err);
      });
  }
  updateClient(client: Client) {
    // console.log(client); // Test JSON
    this.clientcollection.doc(client.id).update(client).then(() => {
      console.log('Atualizou o Serviço'); // Test update client
    }).catch((err) => {
      console.log(err); // Catch error update client
    });
  }

  deleteClient(id: string) {
    // console.log(id); // Test JSON
    this.clientcollection.doc(id).delete()
      .then(result => {
        // console.log(result); // Test delete client
      }).catch((err) => {
        // console.log(err); // Catch error delete client
      });
  }
}
