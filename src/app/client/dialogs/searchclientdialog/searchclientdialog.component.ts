import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {EditclientdialogComponent} from '../editclientdialog/editclientdialog.component';
import {MatDialogRef} from '@angular/material/dialog';
import {ClientService} from '../../client.service';
import {MatTableDataSource} from '@angular/material';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-searchclientdialog',
  templateUrl: './searchclientdialog.component.html',
  styleUrls: ['./searchclientdialog.component.css']
})
export class SearchclientdialogComponent implements OnInit, OnDestroy {
  form: FormGroup;
  dataSource = new MatTableDataSource();
  subs: Subscription;
  displayedColumns = ['name', 'document', 'actions']; // variable for layout arrangement

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditclientdialogComponent>,
    private clientService: ClientService
  ) { }

  ngOnInit() {
    this.subs = this.clientService.fetchAvailableClients(sessionStorage.getItem('userId')).subscribe(resp => {
      this.dataSource.data = resp;
    });
  }

  ngOnDestroy(): void { // unsubscribe from ClientService
    // Called once, before the instance is destroyed.
    this.subs.unsubscribe();
  }

  applyFilter(filterValue: string) { // Filter table
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  submit(form) {
    this.dialogRef.close({

    });
  }

  select(element){
    // console.log(element);
    this.ngOnDestroy();
    this.dialogRef.close({
      selectedClient: element
    });
  }

  onNoClick(): void {
    this.ngOnDestroy();
    this.dialogRef.close();
  }

}
