import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteclientdialogComponent } from './deleteclientdialog.component';

describe('DeletevendordialogComponent', () => {
  let component: DeleteclientdialogComponent;
  let fixture: ComponentFixture<DeleteclientdialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteclientdialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteclientdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
