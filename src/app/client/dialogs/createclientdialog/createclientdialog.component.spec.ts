import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateclientdialogComponent } from './createclientdialog.component';

describe('CreateclientdialogComponent', () => {
  let component: CreateclientdialogComponent;
  let fixture: ComponentFixture<CreateclientdialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateclientdialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateclientdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
