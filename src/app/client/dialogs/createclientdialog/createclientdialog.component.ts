import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-createclientdialog',
  templateUrl: './createclientdialog.component.html',
  styleUrls: ['./createclientdialog.component.css']
})
export class CreateclientdialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<CreateclientdialogComponent>
  ) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: '',
      cnpj: '',
      phone: '',
      street: '',
      number: '',
      district: '',
      city: '',
      state: '',
      email: new FormControl('', {
        validators: [Validators.required, Validators.email]
      })
    });
  }

  submit(form) {
    this.dialogRef.close({
      name: form.value.name,
      cnpj: form.value.cnpj,
      phone: form.value.phone,
      email: form.value.email,
      address: {
        street: form.value.street,
        number: form.value.number,
        district: form.value.district,
        city: form.value.city,
        state: form.value.state,
      },
      createdDate: Date.now()
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
