import {Component, OnDestroy, OnInit} from '@angular/core';
import { AuthService } from '../auth.service';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { UIService } from '../../shared/ui.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  typePerson = '';
  isLoading = false;
  private loadingSubs: Subscription;
  loginForm: FormGroup;

  constructor(
    private authService: AuthService,
    private uiService: UIService
  ) { }

  ngOnInit() {
    this.loadingSubs = this.uiService.loadingStateChanged.subscribe(isLoading => {
      this.isLoading = isLoading;
    });
    this.loginForm = new FormGroup({
      email: new FormControl('', {
        validators: [Validators.required, Validators.email]
      }),
      password: new FormControl('', { validators: [Validators.required] })
    });
  }

  ngOnDestroy() {
    this.loadingSubs.unsubscribe();
  }

  onLogin(form: NgForm) {

    this.authService.login({
      email: form.value.email,
      password: form.value.password
    });
  }

  onSignup(form: NgForm) {
    console.log(form);
    this.authService.registerUser({
      email: form.value.email,
      password: form.value.password,
      tipoPessoa: this.typePerson,
      doc: this.isPerson() ? form.value.cpf : form.value.cnpj,
      nome: this.isPerson() ? form.value.nome : form.value.razaosocial
    });
  }
  resetPassword(form: NgForm) {
    // console.log(form);
    this.authService.resetPassword(form.value.email);
  }
  isCompany() {
    return this.typePerson === 'juridica' ? true : false;
  }
  isPerson() {
    return this.typePerson === 'fisica' ? true : false;
  }
  isReady() {
    return this.typePerson !== '' ? true : false;
  }
}
