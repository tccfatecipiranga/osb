import { Observable } from 'rxjs/Observable';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/take';

import { User } from '../model/user.model';
import { AuthData } from '../model/auth-data.model';
import { AngularFireAuth } from 'angularfire2/auth';
import { AccountService } from '../account/account.service';
import {UIService} from '../shared/ui.service';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class AuthService {

  authChange = new Subject<boolean>();
  private isAuthenticated = false;
  userO: Observable<User>;
  user: User;
  // , private accountService: AccountService
  constructor(
    private router: Router,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private uiService: UIService
  ) {
    this.userO = this.afAuth.authState
      .switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        }
      });
  }

  registerUser(authData: AuthData) {
    this.uiService.loadingStateChanged.next(true);
    this.afAuth.auth
      .createUserWithEmailAndPassword(authData.email, authData.password)
      .then(result => {
        this.uiService.showSnackbar('Usuário foi cadastrado no sistema', null, 3000);
        authData.userId = result.user.uid;
        console.log(authData);
        sessionStorage.setItem('userId', result.user.uid);
        const userRef: AngularFirestoreDocument<any> = this.afs.doc('users/' + authData.userId);
        userRef.set(authData, {merge: true});
        setTimeout(() => {
          this.uiService.loadingStateChanged.next(false);
          this.authSuccessfully();
        }, 2000);
      })
      .catch(error => {
        this.uiService.loadingStateChanged.next(false);
        this.uiService.showSnackbar(error.message, null, 3000);
      });
  }

  login(authData: AuthData) {
    this.uiService.loadingStateChanged.next(true);
    this.afAuth.auth
      .signInWithEmailAndPassword(authData.email, authData.password)
      .then(result => {
        this.uiService.showSnackbar('Abrindo o Sistema', null, 3000);
        console.log(result);
        sessionStorage.setItem('userId', result.user.uid);
        this.isAuthenticated = true;
        setTimeout(() => {
          this.uiService.loadingStateChanged.next(false);
          this.authSuccessfully();
        }, 2000);
      })
      .catch(error => {
        this.uiService.loadingStateChanged.next(false);
        console.log(error);
        switch (error.code) {
          case 'auth/wrong-password': {
            this.uiService.showSnackbar('Email ou senha está errado', null, 3000);
            break;
          }
          case 'auth/network-request-failed': {
            this.uiService.showSnackbar('Problema na conexão de Internet', null, 3000);
            break;
          }
        }
      });
  }
  resetPassword(email: string) {
    this.uiService.loadingStateChanged.next(true);
    this.afAuth.auth.sendPasswordResetEmail(email)
      .then(result => {
        this.uiService.loadingStateChanged.next(false);
        console.log(result);
        this.uiService.showSnackbar('Email para recuperar senha foi enviado!', null, 3000);
      })
      .catch(error => {
        this.uiService.loadingStateChanged.next(false);
        console.log(error);
        this.uiService.showSnackbar('Esse email não está cadastrado em nosso sistema', null, 3000);
      });
  }
  logout() {
    this.afAuth.auth.signOut();
    this.authChange.next(false);
    this.router.navigate(['/login']);
    this.isAuthenticated = false;
  }

  isAuth() {
    return this.isAuthenticated;
  }

  private authSuccessfully() {
    this.isAuthenticated = true;
    this.authChange.next(true);
    this.router.navigate(['/']);
  }

  getId() {
    console.log(this.afAuth.auth.currentUser.uid.toString());
    // return this.afAuth.auth.currentUser.uid.toString();
  }
}
