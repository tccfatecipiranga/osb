// fornecedor
export interface Supplier {
  id?: string;
  companyName: string;
  cnpj: number;
  address: string;
  complement: string;
  neighborhood: string;
  zipNumber: number;
  email: string;
  phone: number;
}
