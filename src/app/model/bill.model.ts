// contas a pagar e receber podemos usar o type para calcular e fazer a busca e exibir na tela
export interface Bill {
  id?: string;
  value: number;
  type: string;
  date: string;
}
