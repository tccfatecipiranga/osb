// classe para criar usuario e que vai receber o observable e etc
export interface User {
  email: string;
  userId: string;
  doc?: string;
  name?: string;
  typePerson?: string;
}
