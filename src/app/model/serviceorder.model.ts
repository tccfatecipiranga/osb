import {Product} from './product.model';
import {Service} from './service.model';
import {ProductType} from './productType.model';
import {Brand} from './brand.model';
import {Client} from './client.model';


export interface ServiceOrder {
  id?: string;
  status?: string;
  client?: Client;
  typeProduct?: ProductType;
  brand?: Brand;
  model?: string;
  serial?: string;
  problem?: string;
  solution?: string;
  approved?: boolean;
  descProblem?: string;
  products?: Product[];
  services?: Service[];
  taxExtra?: number;
  totalValue?: number;
  createdDate?: any;
}
