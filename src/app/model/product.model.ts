// produtos como hd que podem ser adicionados a uma ordem de serviço
import {Brand} from './brand.model';
import {ProductType} from './productType.model';

export interface Product {
  id?: string;
  model?: string;
  description?: string;
  brand?: Brand;
  type?: ProductType;
  priceCost?: number;
  price?: number;
  quantity?: number;
  createdDate?: any;
}
