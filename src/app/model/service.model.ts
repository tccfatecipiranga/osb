// serviços que podem ser adicionados a uma ordem de serviço
export interface Service {
  id?: string;
  name: string;
  value: number;
  createdDate?: any;
}
