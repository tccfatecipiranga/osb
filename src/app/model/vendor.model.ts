import { Address } from '../model/address.model';
// serviços que podem ser adicionados a uma ordem de serviço
export interface Vendor {
  id?: string;
  name: string;
  cnpj?: string;
  email?: string;
  phone?: string;
  address?: Address;

  createdDate?: any;
}
