export interface Client {
  id?: string;
  name: string;
  type: string;
  document: number;
  address: string;
  complement: string;
  neighborhood: string;
  zipNumber: number;
  email: string;
  cellphone: string;
  phone: string;
  clientSince?: string;
}
