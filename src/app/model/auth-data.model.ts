export interface AuthData {
    email?: string;
    password?: string;
    userId?: string;
    doc?: string;
    nome?: string;
    tipoPessoa?: string;
    address?: string;
    complement?: string;
    neighborhood?: string;
    zipNumber?: number;
}
