// main Angular
import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// Custom Components
import {AppComponent} from './app.component';
import {LoginComponent} from './auth/login/login.component';
import {AccountComponent} from './account/account.component';
import {WelcomeComponent} from './welcome/welcome.component';
import {SidenavListComponent} from './navigation/sidenav-list/sidenav-list.component';
import {HeaderComponent} from './navigation/header/header.component';
import {ServiceComponent} from './service/service.component';
import {VendorComponent} from './vendor/vendor.component';
import {OrderserviceComponent} from './orderservice/orderservice.component';
import {ClientComponent} from './client/client.component';
import {BrandComponent} from './brand/brand.component';
import {ProductTypeComponent} from './productType/productType.component';
// Dialogs Components
import {EditservicedialogComponent} from './service/dialogs/editservicedialog/editservicedialog.component';
import {CreateservicedialogComponent} from './service/dialogs/createservicedialog/createservicedialog.component';
import {DeleteservicedialogComponent} from './service/dialogs/deleteservicedialog/deleteservicedialog.component';

import {EditvendordialogComponent} from './vendor/dialogs/editvendordialog/editvendordialog.component';
import {CreatevendordialogComponent} from './vendor/dialogs/createvendordialog/createvendordialog.component';
import {DeletevendordialogComponent} from './vendor/dialogs/deletevendordialog/deletevendordialog.component';

import {EditclientdialogComponent} from './client/dialogs/editclientdialog/editclientdialog.component';
import {CreateclientdialogComponent} from './client/dialogs/createclientdialog/createclientdialog.component';
import {DeleteclientdialogComponent} from './client/dialogs/deleteclientdialog/deleteclientdialog.component';

import {DeletebranddialogComponent} from './brand/dialogs/deletebranddialog/deletebranddialog.component';
import {CreatebranddialogComponent} from './brand/dialogs/createbranddialog/createbranddialog.component';
import {EditbranddialogComponent} from './brand/dialogs/editbranddialog/editbranddialog.component';
// Angular Material
import {MaterialModule} from './material.module';
// Google Firebase
import {environment} from '../environments/environment';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import { AngularFireDatabaseModule } from 'angularfire2/database';
// etc
import {AppRoutingModule} from './app-routing.module';
import {AuthService} from './auth/auth.service';
import {ServiceService} from './service/service.service';
import {BrandService} from './brand/brand.service';
import {VendorService} from './vendor/vendor.service';
import {ClientService} from './client/client.service';
import {registerLocaleData} from '@angular/common';
import localeBr from '@angular/common/locales/pt';
import {UIService} from './shared/ui.service';
import {EditProductTypeDialogComponent} from './productType/dialogs/editProductTypeDialog/editProductTypeDialog.component';
import {CreateProductTypeDialogComponent} from './productType/dialogs/createProductTypeDialog/createProductTypeDialog.component';
import {DeleteProductTypeDialogComponent} from './productType/dialogs/deleteProductTypeDialog/deleteProductTypeDialog.component';
import {ProductTypeService} from './productType/productType.service';
import {ProductService} from './product/product.service';
import {ProductComponent} from './product/product.component';
import {EditProductDialogComponent} from './product/dialogs/editProductDialog/editProductDialog.component';
import {CreateProductDialogComponent} from './product/dialogs/createProductDialog/createProductDialog.component';
import {DeleteProductDialogComponent} from './product/dialogs/deleteProductDialog/deleteProductDialog.component';

import { CreateorderserviceDialogComponent } from './orderservice/dialogs/createorderservicedialog/createorderservicedialog.component';
import { EditorderserviceDialogComponent } from './orderservice/dialogs/editorderservicedialog/editorderservicedialog.component';
import { DeleteorderserviceDialogComponent } from './orderservice/dialogs/deleteorderservicedialog/deleteorderservicedialog.component';
import { OrderserviceService } from './orderservice/orderservice.service';
import { SearchclientdialogComponent } from './client/dialogs/searchclientdialog/searchclientdialog.component';

registerLocaleData(localeBr, 'pt-BR');

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AccountComponent,
    WelcomeComponent,
    HeaderComponent,
    SidenavListComponent,
    OrderserviceComponent,
    ClientComponent,
    ServiceComponent,
    EditservicedialogComponent,
    CreateservicedialogComponent,
    DeleteservicedialogComponent,

    VendorComponent,
    EditvendordialogComponent,
    CreatevendordialogComponent,
    DeletevendordialogComponent,

    ClientComponent,
    EditclientdialogComponent,
    CreateclientdialogComponent,
    DeleteclientdialogComponent,

    BrandComponent,
    EditbranddialogComponent,
    CreatebranddialogComponent,
    DeletebranddialogComponent,

    ProductTypeComponent,
    EditProductTypeDialogComponent,
    CreateProductTypeDialogComponent,
    DeleteProductTypeDialogComponent,

    ProductComponent,
    EditProductDialogComponent,
    CreateProductDialogComponent,
    DeleteProductDialogComponent,

    CreateorderserviceDialogComponent,
    EditorderserviceDialogComponent,
    DeleteorderserviceDialogComponent,
    SearchclientdialogComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule
  ],
  providers: [ // All Services go Here
    AuthService,
    ServiceService,
    VendorService,
    ClientService,
    BrandService,
    ProductTypeService,
    ProductService,
    UIService,
    OrderserviceService,
    {provide: LOCALE_ID, useValue: 'pt-BR'}
  ],
  bootstrap: [AppComponent],
  entryComponents: [ // All Dialogs go Here
    EditservicedialogComponent,
    CreateservicedialogComponent,
    DeleteservicedialogComponent,

    EditbranddialogComponent,
    CreatebranddialogComponent,
    DeletebranddialogComponent,

    EditvendordialogComponent,
    CreatevendordialogComponent,
    DeletevendordialogComponent,

    EditclientdialogComponent,
    CreateclientdialogComponent,
    DeleteclientdialogComponent,

    EditProductTypeDialogComponent,
    CreateProductTypeDialogComponent,
    DeleteProductTypeDialogComponent,

    EditProductDialogComponent,
    CreateProductDialogComponent,
    DeleteProductDialogComponent,

    CreateorderserviceDialogComponent,
    EditorderserviceDialogComponent,
    DeleteorderserviceDialogComponent,
    SearchclientdialogComponent
  ]
})
export class AppModule {
}
