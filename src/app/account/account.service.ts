import { User } from '../model/user.model';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/take';
import {AngularFireAuth} from 'angularfire2/auth';

@Injectable()
export class AccountService {
  user: User;
  userO: Observable<User>;
  constructor(private router: Router, private afAuth: AngularFireAuth, private afs: AngularFirestore) {
    this.userO = this.afAuth.authState
      .switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return Observable.of(null);
        }
      });
  }
  addDetails(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc('users/' + user.userId);

    const data: User = {
      email: user.email,
      userId: user.userId,
      doc: user.doc,
      name: user.name,
      typePerson: user.typePerson
    };
    userRef.set(data, {merge: true});
    this.router.navigate(['/']);
  }
  updateDetails(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc('users/' + user.userId);
    const data: User = {
      email: user.email,
      userId: user.userId,
      doc: user.doc,
      name: user.name,
      typePerson: user.typePerson
    };
    userRef.update(data);
  }
}
