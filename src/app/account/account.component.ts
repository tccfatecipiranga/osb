import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { User } from '../model/user.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  typeCompany: boolean;
  typePerson: boolean;
  nextStep: boolean;
  userT: User;

  userDocument: AngularFirestoreDocument<any> = this.afs.doc('users/' + sessionStorage.getItem('userId'));
  userobs = this.userDocument.valueChanges();

  constructor(private router: Router, private afs: AngularFirestore) {

  }

  ngOnInit() {

  }

  onSubmit(form: NgForm) {
    this.userDocument.update({
      email: form.value.email,
      tipoPessoa: form.value.fisico ? 'fisica' : 'juridica',
      doc: this.isPerson() ? form.value.cpf : form.value.cnpj,
      nome: this.isPerson() ? form.value.nome : form.value.razaosocial
    }).then(() => {
      console.log('Atualizou o Serviço');
    }).catch( (err) => {
      console.log(err);
    });
  }

  setCompany() {
    this.typeCompany = true;
    this.typePerson = false;
    this.nextStep = true;
  }

  setPerson() {
    this.typeCompany = false;
    this.typePerson = true;
    this.nextStep = true;
  }

  isCompany() {
    return this.typeCompany;
  }

  isPerson() {
    return this.typePerson;
  }
  isReady() {
    return this.nextStep;
  }

}
