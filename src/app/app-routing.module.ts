import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccountComponent} from './account/account.component';
import {WelcomeComponent} from './welcome/welcome.component';
import {LoginComponent} from './auth/login/login.component';
import {ClientComponent} from './client/client.component';
import {OrderserviceComponent} from './orderservice/orderservice.component';
import {ProductComponent} from './product/product.component';
import {ServiceComponent} from './service/service.component';
import {VendorComponent} from './vendor/vendor.component';
import {BrandComponent} from './brand/brand.component';
import {AuthGuard} from './auth/auth.guard';
import {ProductTypeComponent} from './productType/productType.component';

const routes: Routes = [
  {path: '', component: WelcomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'account', component: AccountComponent, canActivate: [AuthGuard]},
  {path: 'services', component: ServiceComponent, canActivate: [AuthGuard]},
  {path: 'vendors', component: VendorComponent, canActivate: [AuthGuard]},
  {path: 'product', component: ProductComponent, canActivate: [AuthGuard]},
  {path: 'orderService', component: OrderserviceComponent, canActivate: [AuthGuard]},
  {path: 'client', component: ClientComponent, canActivate: [AuthGuard]},
  {path: 'brand', component: BrandComponent, canActivate: [AuthGuard]},
  {path: 'product-type', component: ProductTypeComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {
}
