// Angular Components
  import { Component, OnInit, Inject, OnDestroy } from '@angular/core';

// Material Components
  import { MatTableDataSource } from '@angular/material';
  import { MatDialog } from '@angular/material';

// Firebase Components
  import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

// Custom Components
  import { EditservicedialogComponent } from './dialogs/editservicedialog/editservicedialog.component';
  import { CreateservicedialogComponent } from './dialogs/createservicedialog/createservicedialog.component';
  import { DeleteservicedialogComponent } from './dialogs/deleteservicedialog/deleteservicedialog.component';

// etc
  import { ServiceService } from './service.service';
  import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})

export class ServiceComponent implements OnInit, OnDestroy {
  dataSource = new MatTableDataSource(); // variable for DataTable sa
  subs: Subscription; // variable for retrieval of data
  displayedColumns = ['name', 'value', 'created', 'actions']; // variable for layout arrangement
  // uid = this.authService.getId;

  constructor(
    private dialog: MatDialog, // variable used to instantiate dialog
    private ss: ServiceService, // variable for access to CRUD service of Services
    private authService: AuthService
  ) {}

  ngOnInit() {
    // Called once, on initialization of the instance
    this.subs = this.ss.fetchAvailableServices(sessionStorage.getItem('userId')).subscribe(resp => {
      this.dataSource.data = resp; // Fill Data from subscription to data in dataSource
    });
    // console.log('Retrieval of data complete'); // Log Data Retrieval
  }
  ngOnDestroy(): void { // unsubscribe from serviceservice
    // Called once, before the instance is destroyed.
    this.subs.unsubscribe();
  }

  applyFilter(filterValue: string) { // Filter table
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  add() {
    const createDialog = this.dialog.open(CreateservicedialogComponent, { });
    createDialog.afterClosed().subscribe(service => {
      if (service) {
        // console.log(form.value.name); // Test return of Dialog
        this.ss.addService(service);
      }
    });
  }

  delete(id: string, name: string) {
    const deleteDialog = this.dialog.open(DeleteservicedialogComponent, {
      data: {
        name: name
      }
    });
    deleteDialog.afterClosed().subscribe(resp => {
      if (resp) {
        // console.log(resp); // Test return of Dialog
        this.ss.deleteService(id);
      }
    });
  }

  update(i: number, id: string, name: string, value: number) {
    this.authService.getId();
    const editDialog = this.dialog.open(EditservicedialogComponent, {
      data: {
        id: id,
        name: name,
        value: value
      }
    });
    editDialog.afterClosed().subscribe(service => {
      if (service) {
        // console.log(form); // Test return of Dialog
        this.ss.updateService(service);
      }
    });
  }


}
