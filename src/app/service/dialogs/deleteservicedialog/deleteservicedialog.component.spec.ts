import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteservicedialogComponent } from './deleteservicedialog.component';

describe('DeleteservicedialogComponent', () => {
  let component: DeleteservicedialogComponent;
  let fixture: ComponentFixture<DeleteservicedialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteservicedialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteservicedialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
