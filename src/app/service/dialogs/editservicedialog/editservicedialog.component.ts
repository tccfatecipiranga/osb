import { Component, OnInit, Inject, NgModule } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-seditdialog',
  templateUrl: './editservicedialog.component.html',
  styleUrls: ['./editservicedialog.component.css']
})
export class EditservicedialogComponent implements OnInit {
  form: FormGroup;
  nameOld: string;
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditservicedialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      id: this.data.id,
      name: this.data.name,
      nameOld: this.data.name,
      value: this.data.value
    });
  }
  submit(form) {
    this.dialogRef.close({
      id: this.data.id,
      name: form.value.name,
      value: form.value.value
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
