import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateservicedialogComponent } from './createservicedialog.component';

describe('CreateservicedialogComponent', () => {
  let component: CreateservicedialogComponent;
  let fixture: ComponentFixture<CreateservicedialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateservicedialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateservicedialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
