import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-createservicedialog',
  templateUrl: './createservicedialog.component.html',
  styleUrls: ['./createservicedialog.component.css']
})
export class CreateservicedialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<CreateservicedialogComponent>
  ) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: '',
      value: ''
    });
  }

  submit(form) {
    this.dialogRef.close({
      name: form.value.name,
      value: form.value.value,
      createdDate: Date.now()
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
