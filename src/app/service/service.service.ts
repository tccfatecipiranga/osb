import {Injectable} from '@angular/core';
import {Service} from '../model/service.model';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {MatTableDataSource} from '@angular/material';
import {Subject} from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from '@firebase/util';

@Injectable()
export class ServiceService {
  servicecollection: AngularFirestoreCollection<any> = this.afs.collection('users/' + sessionStorage.getItem('userId') + '/services/');
  // serviceChanged = new Subject<Service>();
  // servicesChanged = new Subject<Service[]>();
  // private services: Service[] = [];
  constructor(
    private afs: AngularFirestore
  ) {}

  fetchAvailableServices(userId: string): any {
    return this.afs
    .collection('users/' + userId + '/services/')
    .snapshotChanges()
    .map(items => {
      return items.map(doc => {
        // console.log(doc.payload.doc.data().name); test retrive of items
        return {
          id: doc.payload.doc.id,
          name: doc.payload.doc.data().name,
          value: doc.payload.doc.data().value,
          createdDate: doc.payload.doc.data().createdDate
        };
      });
    });
    // .subscribe((resp: Service[]) => {
    //   this.services = resp;
    //   this.servicesChanged.next([...this.services]);
    // });
  }

  addService(service: Service) {
    // console.log(service); // Test JSON
    this.servicecollection.add(service)
    .then(() => {
      console.log('Gravou Serviço'); // Test add service
    }).catch((err) => {
      console.log(err);
    });
  }

  updateService(service: Service) {
    // console.log(service); // Test JSON
    this.servicecollection.doc(service.id).update(service).then(() => {
      console.log('Atualizou o Serviço'); // Test update service
    }).catch( (err) => {
      console.log(err); // Catch error update service
    });
  }

  deleteService(id: string) {
    // console.log(id); // Test JSON
    this.servicecollection.doc(id).delete()
    .then(result => {
      // console.log(result); // Test delete service
    }).catch((err) => {
      // console.log(err); // Catch error delete service
    });
  }
}
