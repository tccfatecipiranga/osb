// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCMhGELk4KGj8iEbEYyWRvY_yt6niy6BzA',
    authDomain: 'osbr-fatec-ipi.firebaseapp.com',
    databaseURL: 'https://osbr-fatec-ipi.firebaseio.com',
    projectId: 'osbr-fatec-ipi',
    storageBucket: 'osbr-fatec-ipi.appspot.com',
    messagingSenderId: '764156577914'
  }
};
