export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCMhGELk4KGj8iEbEYyWRvY_yt6niy6BzA',
    authDomain: 'osbr-fatec-ipi.firebaseapp.com',
    databaseURL: 'https://osbr-fatec-ipi.firebaseio.com',
    projectId: 'osbr-fatec-ipi',
    storageBucket: 'osbr-fatec-ipi.appspot.com',
    messagingSenderId: '764156577914'
  }
};
